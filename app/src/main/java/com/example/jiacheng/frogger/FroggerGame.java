package com.example.jiacheng.frogger;

import java.util.Random;

/**
 * The game has two internal coordinates:
 * 1. The lane index, ranging from 0 to 12, which shall be translated into y of the final GUI.
 * 2. The x, ranging as if the game is in a view with width 1080, which shall be translated into x
 *    of the final GUI.
 *
 * Any GUI design should be able to access all the needed information through an instance of this
 * class.
 */
class FroggerGame {
    private Random random = new Random();

    Frog frog;

    /**
     * The game has 13 lanes. Lanes 0, 6 and 12 are empty, lanes 1-5 are the river, lanes 7-11 are
     * the road.
     */
    Lane[] lanes = new Lane[13];

    boolean frogIsAlive;

    private int generateNewSpeed(){ return random.nextInt(3) + 1; }
    private int generateNewWoodLength(){ return random.nextInt(241) + 120; }
    private int generateNewWoodDistance(){ return random.nextInt(481) + 240; }
    private int generateNewCarLength(){ return random.nextInt(121) + 120; }
    private int generateNewCarDistance(){ return random.nextInt(121) + 180; }


    /**
     * The game is initialized as:
     *
     * 0     destination lane
     * 1     river lane with woods
     * ...   ...
     * 5     river lane with woods
     * 6     empty lane
     * 7     road lane with cars
     * ...   ...
     * 11    road lane with cars
     * 12    start lane with a frog in the middle
     *
     * with randomly generated moving objects having random speeds and distances.
     */
    FroggerGame(){
        frog = new Frog(1080/2, 12);
        frogIsAlive = true;

        for(int laneIndex = 0; laneIndex < 13; laneIndex ++){
            int speed = generateNewSpeed();
            if(laneIndex%2 == 0 ^ laneIndex > 6){
                speed = -speed;
            }
            lanes[laneIndex] = new Lane(laneIndex, speed);
        }

        for(int laneIndex = 1; laneIndex < 6; laneIndex ++){
            Lane lane = lanes[laneIndex];
            int nextWoodLeftEnd = -120;
            while(nextWoodLeftEnd < 1080){
                int newWoodLength = generateNewWoodLength();
                lane.movingObjects.add(new Wood(nextWoodLeftEnd + newWoodLength/2, newWoodLength, lane));
                nextWoodLeftEnd += newWoodLength;
                nextWoodLeftEnd += generateNewWoodDistance();
            }
        }

        for(int laneIndex = 7; laneIndex < 12; laneIndex ++){
            Lane lane = lanes[laneIndex];
            int nextCarLeftEnd = -120;
            while(nextCarLeftEnd < 1080){
                int newCarLength = generateNewCarLength();
                lane.movingObjects.add(new Car(nextCarLeftEnd + newCarLength/2, newCarLength, lane));
                nextCarLeftEnd += newCarLength;
                nextCarLeftEnd += generateNewCarDistance();
            }
        }
    }

    private int nextWoodDistance = generateNewWoodDistance();
    private int nextCarDistance = generateNewCarDistance();
    void run(){
        int frogLaneIndex = frog.laneIndex;

        // See whether the frog will die now.

        if(frogIsAlive){
            if(frogLaneIndex >= 1 && frogLaneIndex < 6){
                boolean frogWillDrawn = true;
                for(MovingObject wood: lanes[frogLaneIndex].movingObjects){
                    if(Math.abs(frog.x - wood.x) <= (wood.length/2 - Frog.width/2)){
                        frogWillDrawn = false;
                    }
                }
                frogIsAlive = !frogWillDrawn;
            }
            else if(frogLaneIndex >= 7 && frogLaneIndex < 12){
                boolean frogWillCrash = false;
                for(MovingObject car: lanes[frogLaneIndex].movingObjects){
                    if(Math.abs(frog.x - car.x) <= (car.length/2 + Frog.width/2)){
                        frogWillCrash = true;
                    }
                }
                frogIsAlive = !frogWillCrash;
            }
        }

        if(frogIsAlive){
            // Then make all MovingObjects and the frog run.

            for(Lane lane: lanes){
                for(MovingObject movingObject: lane.movingObjects){
                    movingObject.run();
                }
            }
            if(frogLaneIndex > 0 && frogLaneIndex < 6){
                frog.x += lanes[frogLaneIndex].speed;
            }

            // Remove unneeded woods from the river, and add new woods at a pre-generated distance.
            // After a wood is generated, renew nextWoodDistance.

            for(int laneIndex = 1; laneIndex < 6; laneIndex ++){
                Lane lane = lanes[laneIndex];
                for(MovingObject wood: lane.movingObjects){
                    if(wood.x < -wood.length/2 || wood.x > 1079 + wood.length/2){
                        lane.movingObjects.remove(wood);
                        break;
                    }
                }

                boolean movingTowardsRight = lane.speed > 0;
                int newWoodLength = generateNewWoodLength();
                MovingObject newWood = new Wood(
                        movingTowardsRight ? (0 - newWoodLength/2) : (1079 + newWoodLength/2),
                        newWoodLength, lane
                );
                boolean tooCloseToAnExistingWood = false;
                for(MovingObject wood: lane.movingObjects){
                    if((Math.abs(wood.x - newWood.x) - wood.length/2 - newWood.length/2) < nextWoodDistance){
                        tooCloseToAnExistingWood = true;
                    }
                }
                if(!tooCloseToAnExistingWood){
                    lane.movingObjects.add(newWood);
                    nextWoodDistance = generateNewWoodDistance();
                }
            }

            // Remove unneeded cars from the road, and add new cars at a pre-generated distance.
            // After a car is generated, renew nextCarDistance.

            for(int laneIndex = 7; laneIndex < 12; laneIndex ++){
                Lane lane = lanes[laneIndex];
                for(MovingObject car: lane.movingObjects){
                    if(car.x < -car.length/2 || car.x > 1079 + car.length/2){
                        lane.movingObjects.remove(car);
                        break;
                    }
                }

                boolean movingTowardsRight = lane.speed > 0;
                int newCarLength = generateNewCarLength();
                MovingObject newCar = new Car(
                        movingTowardsRight ? (0 - newCarLength/2) : (1079 + newCarLength/2),
                        newCarLength, lane
                );
                boolean tooCloseToAnExistingCar = false;
                for(MovingObject car: lane.movingObjects){
                    if((Math.abs(car.x - newCar.x) - car.length/2 - newCar.length/2) < nextCarDistance){
                        tooCloseToAnExistingCar = true;
                    }
                }
                if(!tooCloseToAnExistingCar){
                    lane.movingObjects.add(newCar);
                    nextCarDistance = generateNewCarDistance();
                }
            }
        }
    }

    void jump(Direction direction){
        if(frogIsAlive) frog.jump(direction);
    }
}
