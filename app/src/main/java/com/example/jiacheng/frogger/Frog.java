package com.example.jiacheng.frogger;

class Frog {
    private static final int JUMP_RANGE_X = 60;
    static final int width = 60;

    int x;
    int laneIndex;

    Frog(int x, int laneIndex){
        this.x = x;
        this.laneIndex = laneIndex;
    }

    void jump(Direction direction){
        switch (direction){
            case LEFT:
                if(x >= JUMP_RANGE_X) x -= JUMP_RANGE_X;
                break;
            case RIGHT:
                if(x <= 1079 - JUMP_RANGE_X) x += JUMP_RANGE_X;
                break;
            case FORWARD:
                if(laneIndex >= 1) laneIndex -= 1;
                break;
            case BACKWARD:
                if(laneIndex <= 11)laneIndex += 1;
                break;
        }
    }
}
