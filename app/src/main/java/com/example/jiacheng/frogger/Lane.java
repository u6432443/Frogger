package com.example.jiacheng.frogger;

import java.util.ArrayList;

class Lane {
    int laneIndex;
    int speed;
    ArrayList<MovingObject> movingObjects;

    Lane(int laneIndex, int speed){
        this.laneIndex = laneIndex;
        this.speed = speed;
        movingObjects = new ArrayList<>();
    }
}
