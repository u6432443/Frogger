package com.example.jiacheng.frogger;

enum Direction {
    FORWARD, BACKWARD, LEFT, RIGHT
}
