package com.example.jiacheng.frogger;

class MovingObject {
    int x;
    int length;
    Lane lane;

    MovingObject(int x, int length, Lane lane){
        this.x = x;
        this.length = length;
        this.lane = lane;
    }

    void run(){
        x += lane.speed;
    }
}
