package com.example.jiacheng.frogger;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class GameView extends View implements GestureDetector.OnGestureListener, View.OnTouchListener, Runnable {
    GestureDetector gestureDetector;

    Handler timer;

    Paint p;

    FroggerGame froggerGame;

    int width = 0;
    int height;
    float xScale;
    int yScale;

    public GameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setOnTouchListener(this);
        gestureDetector = new GestureDetector(context, this);

        timer = new Handler();
        timer.postDelayed(this,20);

        p = new Paint();
        p.setTextSize(100);

        froggerGame = new FroggerGame();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(width == 0){
            width = getWidth();
            height = getHeight();
            xScale = (float) width/ 1080.0f;
            yScale = height/13;
        }

        for(int laneIndex = 1; laneIndex < 6; laneIndex ++){
            p.setColor(Color.rgb(0, 102, 204));
            canvas.drawRect(0, laneIndex*yScale, width, (laneIndex+1)*yScale, p);
            Lane lane = froggerGame.lanes[laneIndex];
            p.setColor(Color.rgb(139, 69, 19));
            for(MovingObject wood: lane.movingObjects){
                canvas.drawRect((wood.x - wood.length/2)*xScale, laneIndex*yScale + height/130,
                        (wood.x + wood.length/2)*xScale, (laneIndex+1)*yScale - height/130, p);
            }
        }
        for(int laneIndex = 7; laneIndex < 12; laneIndex ++){
            p.setColor(Color.LTGRAY);
            canvas.drawRect(0, laneIndex*yScale, width, (laneIndex+1)*yScale, p);
            Lane lane = froggerGame.lanes[laneIndex];
            p.setColor(Color.BLACK);
            for(MovingObject car: lane.movingObjects){
                canvas.drawRect((car.x - car.length/2)*xScale, laneIndex*yScale + height/130,
                        (car.x + car.length/2)*xScale, (laneIndex+1)*yScale - height/130, p);
            }
        }

        p.setColor(Color.GREEN);
        canvas.drawRect((froggerGame.frog.x - Frog.width/2)*xScale,
                froggerGame.frog.laneIndex*yScale + height/130, (froggerGame.frog.x + Frog.width/2)*xScale,
                (froggerGame.frog.laneIndex + 1)*yScale - height/130, p);

        if(!froggerGame.frogIsAlive){
            p.setColor(Color.RED);
            canvas.drawText("YOU ARE DEAD", 0, height/2, p);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event){
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e){
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        float moveRight = e2.getX() - e1.getX();
        float moveDown = e2.getY() -e1.getY();
        if(Math.abs(moveRight) >= Math.abs(moveDown)){
            if(moveRight >= 0) froggerGame.jump(Direction.RIGHT);
            else froggerGame.jump(Direction.LEFT);
        }
        else{
            if(moveDown >= 0) froggerGame.jump(Direction.BACKWARD);
            else froggerGame.jump(Direction.FORWARD);
        }
        this.invalidate();
        return true;
    }

    @Override
    public void	onLongPress(MotionEvent e){
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
        return false;
    }

    @Override
    public void	onShowPress(MotionEvent e){
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return false;
    }

    @Override
    public void run() {
        froggerGame.run();
        this.invalidate();
        timer.postDelayed(this,20);
    }
}
